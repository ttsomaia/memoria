import {applyMiddleware} from "redux"
import requestHandlerMiddleware from "./requestHandlerMiddleware"

export default applyMiddleware(
  requestHandlerMiddleware,
)
