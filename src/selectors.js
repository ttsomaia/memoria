import {getConfigurationValue, getNestedConfigurationValue} from 'redux-configuration-manager'
import {createSelector} from "reselect"
import {getPreferences} from "./ducks"

export const getDefaultPreferences = getConfigurationValue('defaultPreferences')
export const getMergedPreferences = createSelector(
  getPreferences,
  getDefaultPreferences,
  (accountPreferences, defaultPreferences) => ({
    ...defaultPreferences,
    ...accountPreferences,
  })
)
export const createPreferenceAccessor = (name) => (state) => getMergedPreferences(state)[name]
export const getLocale = createPreferenceAccessor('locale')
export const getApiScheme = getNestedConfigurationValue('api.scheme')
export const getApiHost = getNestedConfigurationValue('api.host')
