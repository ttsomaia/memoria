const getSortedIndex = (array, value, compare) => {
  let low = 0
  let high = array.length

  while (low < high) {
    const mid = (low + high) >>> 1
    compare(array[mid], value) < 0
      ? (low = mid + 1)
      : (high = mid)
  }

  return low
}

export default getSortedIndex
