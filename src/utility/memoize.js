const memoize = (fn, extract) => {
  const cache = new Map()

  if (!extract) {
    return (arg, ...args) => {
      if (cache.has(arg)) {
        return cache.get(arg)
      }

      const result = fn(arg, ...args)
      cache.set(arg, result)

      return result
    }
  }

  return (...args) => {
    const key = extract(...args)

    if (cache.has(key)) {
      return cache.get(key)
    }

    const result = fn(key, ...args)
    cache.set(key, result)

    return result
  }
}

export default memoize
