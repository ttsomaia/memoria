class Range {
  constructor(min, max, step = 1) {
    this.min = min
    this.max = max
    this.step = step
  }

  map(map) {
    const max = this.max
    const step = this.step
    const mapped = []
    for (let c = this.min, i = 0; c <= max; c += step, ++i) {
      mapped.push(map(c, i))
    }

    return mapped
  }

  reduce(reduce, ...initialReduction) {
    const max = this.max
    const step = this.step
    let reduction = initialReduction.length === 0 ? this.min : initialReduction[0]
    for (let c = this.min, i = 0; c <= max; c += step, ++i) {
      reduction = reduce(reduction, c, i)
    }

    return reduction
  }
}

const range = (min, max, step = 1) => new Range(min, max, step)

export default range
