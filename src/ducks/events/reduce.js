import {combineReducers} from 'redux'

import createQueryReducer from '../../createQueryReducer'
import createMutationReducer from "../../createMutationReducer"
import {
  EVENTS_LIST_INVALIDATE,
  EVENTS_LIST_SUCCESS,
  EVENTS_LIST_FAILURE,
  EVENTS_LIST_REQUEST,
  EVENTS_CREATE_SUCCESS,
  EVENTS_CREATE_FAILURE,
  EVENTS_CREATE_REQUEST,
  EVENTS_UPDATE_SUCCESS,
  EVENTS_UPDATE_FAILURE,
  EVENTS_UPDATE_REQUEST,
  EVENTS_DELETE_SUCCESS,
  EVENTS_DELETE_FAILURE,
  EVENTS_DELETE_REQUEST,
} from './actionTypes'

const mapEventToLocalEvent = ({id, name: title, desc: description, color, photo: image, startDate, startTime, endDate, endTime}) => ({
  id,
  title,
  description,
  color,
  image,
  startDate: `${startDate} ${startTime}`,
  endDate: `${endDate} ${endTime}`,
})

const mergeEvents = (prevData, newData) => prevData ? [
  ...prevData,
  ...newData.map(mapEventToLocalEvent),
] : newData.map(mapEventToLocalEvent)

export default combineReducers({
  list: createQueryReducer({
    request: EVENTS_LIST_REQUEST,
    receive: EVENTS_LIST_SUCCESS,
    error: EVENTS_LIST_FAILURE,
    invalidate: EVENTS_LIST_INVALIDATE,
    [EVENTS_CREATE_SUCCESS]: (state, action) => [
      ...state,
      mapEventToLocalEvent(action.payload.body),
    ],
    [EVENTS_UPDATE_SUCCESS]: (state, action) => [
      ...state.filter(item => item.id !== action.payload.body.id),
      mapEventToLocalEvent(action.payload.body),
    ],
    [EVENTS_DELETE_REQUEST]: (state, {payload: {params: {id}}}) => state.filter(item => item.id !== id),
  }, [], mergeEvents),
  create: createMutationReducer({
    request: EVENTS_CREATE_REQUEST,
    success: EVENTS_CREATE_SUCCESS,
    failure: EVENTS_CREATE_FAILURE,
  }),
  update: createMutationReducer({
    request: EVENTS_UPDATE_REQUEST,
    success: EVENTS_UPDATE_SUCCESS,
    failure: EVENTS_UPDATE_FAILURE,
  }),
  delete: createMutationReducer({
    request: EVENTS_DELETE_REQUEST,
    success: EVENTS_DELETE_SUCCESS,
    failure: EVENTS_DELETE_FAILURE,
  }),
})
