export const mapObject = (obj, mapper) => Object.keys(obj).reduce((previous, key) => {
  previous[key] = mapper(obj[key], key, obj)
  return previous
}, {})

export const extractDateFromMoment = (moment) => moment.format('YYYY-MM-DD')

export const extractTimeFromMoment = (moment) => moment.format('HH:mm')

export const createFormData = (data) => {
  const formData = new FormData()

  for(const name of Object.keys(data)) {
    formData.append(name, data[name]);
  }

  return  formData
}
