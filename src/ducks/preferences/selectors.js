export const getPreferences = (state) => state.preferences
export const createPreferenceAccessor = (name) => (state) => state.preferences[name]
