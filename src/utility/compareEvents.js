import compare from './compare'
import {DAY} from '../constants'

const compareEvents = (a, b) => {
  const aStart = +a.startDate
  const aEnd = +a.endDate
  const bStart = +b.startDate
  const bEnd = +b.endDate
  
  if (aEnd - aStart >= DAY) {
    return -1
  }

  if (bEnd - bStart >= DAY) {
    return 1
  }

  return compare(aStart, bStart) || compare(aEnd, bEnd)
}

export default compareEvents
