import {combineReducers} from "redux"
import {persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import createLocalizationReducer from "redux-l10n"
import createConfigurationReducer from 'redux-configuration-manager'
import config from "./config"
import en_US from "./l10n/en_US"
import ka from "./l10n/ka"
import {events, preferences} from './ducks'

const localization = createLocalizationReducer({
  en_US,
  ka,
}, config.defaultPreferences.locale)

export default combineReducers({
  configuration: createConfigurationReducer(config),
  localization: persistReducer({
    key: 'localization.defaultLocale',
    whitelist: ['defaultLocale'],
    storage,
  }, localization),
  events,
  preferences,
})
