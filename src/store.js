import {createStore} from "redux"
import reduce from "./reduce"
import enhanceStore from "./enhanceStore"

export default createStore(reduce, enhanceStore)
