import getNumberOfDaysInMonth from "./getNumberOfDaysInMonth"
import generateDays from "./generateDays"

const generateCalendarMatrix = (date, onDoubleClick, onMouseUp) => {
  const time = date.getTime()
  const startingDayOfWeek = date.getDay()
  const year = date.getFullYear()
  const month = date.getMonth()
  const numberOfDays = getNumberOfDaysInMonth(year, month)
  const previousInitialDay = getNumberOfDaysInMonth(year, month - 1) - (startingDayOfWeek - 1)
  const previousTime = new Date(year, month - 1, previousInitialDay).getTime()
  const nextTime = new Date(year, month + 1, 1).getTime()

  return [
    ...generateDays(
      previousTime,
      startingDayOfWeek,
      getNumberOfDaysInMonth(year, month - 1) - startingDayOfWeek,
      onDoubleClick,
      onMouseUp,
      'day-inactive',
    ),
    ...generateDays(
      time,
      numberOfDays,
      0,
      onDoubleClick,
      onMouseUp,
      'day-active',
    ),
    ...generateDays(
      nextTime,
      42 - (numberOfDays + startingDayOfWeek),
      0,
      onDoubleClick,
      onMouseUp,
      'day-inactive',
    ),
  ]
}

export default generateCalendarMatrix
