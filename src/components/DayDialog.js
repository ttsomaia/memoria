import React from 'react'
import Dialog from "@material-ui/core/es/Dialog/Dialog"
import DialogTitle from "@material-ui/core/es/DialogTitle/DialogTitle"
import DialogContent from "@material-ui/core/es/DialogContent/DialogContent"
import Table from "@material-ui/core/es/Table/Table"
import TableBody from "@material-ui/core/es/TableBody/TableBody"
import TableRow from "@material-ui/core/es/TableRow/TableRow"
import TableCell from "@material-ui/core/es/TableCell/TableCell"
import DialogActions from "@material-ui/core/es/DialogActions/DialogActions"
import Button from "@material-ui/core/es/Button/Button"
import DayEvent from "./DayEvent"
import {range} from "../utility"

export default class DayDialog extends React.PureComponent {
  state = {
    events: {},
    maxEvents: 0,
  }

  constructor(props) {
    super(props)
    const {min, max} = props.timeRange || {}
    this.state.range = range(min ? min.hours : 0, max ? max.hours : 23)
  }

  componentDidUpdate(prevProps) {
    if (this.props.events !== prevProps.events) {
      const events = {}
      for (const event of this.props.events) {
        const {id, title, color, startDate, endDate} = event.props
        const startHours = startDate.hours()
        const endHours = endDate.hours()
        const delta = endHours - startHours

        if (!events[startHours]) {
          events[startHours] = []
        }

        const ev = events[startHours]
        ev.push({
          id,
          title,
          color,
          startDate,
          endDate,
          startHours,
          endHours,
          delta,
          onDoubleClick: (e) => this.props.onEventDoubleClick && this.props.onEventDoubleClick(e, event.props),
        })
      }

      this.setState({events})
    }
  }

  renderDay = (hours) => {
    const {translate} = this.props
    const {events} = this.state
    const ev = events[hours]

    return (
      <TableRow key={hours} classes={classes.row}>
        <TableCell width="50" classes={classes.cell}>{translate('hour', hours)}</TableCell>
        {ev && ev.map(DayEvent)}
      </TableRow>
    )
  }

  renderDayBackdrop = (hours) => (
    <TableRow key={hours} classes={classes.row}>
      <TableCell classes={classes.cell}/>
    </TableRow>
  )

  handleCreateEventClick = () => this.props.onCreateEventClick && this.props.onCreateEventClick(this.props.date)

  render() {
    const {translate, open, title, onClose} = this.props
    const {range} = this.state

    return (
      <Dialog open={open} onClose={onClose} aria-labelledby="event-dialog-title" disableEnforceFocus fullWidth maxWidth={'md'}>
        <DialogTitle id="event-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <div className={'relative'}>
            <Table classes={classes.tableBackdrop}>
              <TableBody>
                {range.map(this.renderDayBackdrop)}
              </TableBody>
            </Table>
            <Table classes={classes.table}>
              <TableBody>
                {range.map(this.renderDay)}
              </TableBody>
            </Table>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleCreateEventClick} color="primary" autoFocus>{translate('create_event')}</Button>
        </DialogActions>
      </Dialog>
    )
  }
}

const classes = {
  tableBackdrop: {
    root: 'day-event-table-backdrop',
  },
  table: {
    root: 'day-event-table',
  },
  row: {
    root: 'event-row',
  },
  cell: {
    root: 'event-table-cell',
  },
}
