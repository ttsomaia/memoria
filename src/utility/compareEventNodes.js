import compare from './compare'

const compareEventNodes = ({props: a}, {props: b}) => {
  if (!a.isFirstDay || !a.isLastDay) {
    return -1
  }

  if (b.isFirstDay && !b.isLastDay) {
    return 1
  }

  return compare(a.startTimestamp, b.startTimestamp) || compare(a.endTimestamp, b.endTimestamp)
}

export default compareEventNodes
