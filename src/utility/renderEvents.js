import renderEvent from './renderEvent'
import compareEvents from './compareEvents'

const renderEvents = (events, onMouseDown, onEventDoubleClick) => {
  const renderedEvents = {}
  events
  .sort(compareEvents)
  .map(event => renderEvent(event, onMouseDown, onEventDoubleClick))
  .forEach(map => Object.keys(map).forEach(timestamp => {
    if (!renderedEvents[timestamp]) {
      renderedEvents[timestamp] = []
    }
    renderedEvents[timestamp].push(map[timestamp])
  }))

  return renderedEvents
}

export default renderEvents
