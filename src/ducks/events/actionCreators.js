import {get, post, del} from 'redux-net'
import moment from "moment"
import {
  EVENTS_LIST_INVALIDATE,
  EVENTS_LIST_SUCCESS,
  EVENTS_LIST_FAILURE,
  EVENTS_LIST_REQUEST,
  EVENTS_CREATE_SUCCESS,
  EVENTS_CREATE_FAILURE,
  EVENTS_CREATE_REQUEST,
  EVENTS_UPDATE_SUCCESS,
  EVENTS_UPDATE_FAILURE,
  EVENTS_UPDATE_REQUEST,
  EVENTS_DELETE_SUCCESS,
  EVENTS_DELETE_FAILURE,
  EVENTS_DELETE_REQUEST,
} from "./actionTypes"
import {createFormData, extractDateFromMoment, extractTimeFromMoment} from "../../helpers"

export const fetchEvents = (month) => get(`/events/month/${month}`, {
  begin: EVENTS_LIST_REQUEST,
  success: EVENTS_LIST_SUCCESS,
  error: EVENTS_LIST_FAILURE,
})

export const invalidateEvents = () => ({
  type: EVENTS_LIST_INVALIDATE,
})

export const createEvent = ({title: name, description: desc, color, image: photo, startDate, endDate}) => {
  const startMoment = moment(startDate)
  const endMoment = moment(endDate)

  return post(`/events/new`, {
    begin: EVENTS_CREATE_REQUEST,
    success: EVENTS_CREATE_SUCCESS,
    error: EVENTS_CREATE_FAILURE,
    body: createFormData({
      name,
      desc,
      color,
      photo,
      startDate: extractDateFromMoment(startMoment),
      startTime: extractTimeFromMoment(startMoment),
      endDate: extractDateFromMoment(endMoment),
      endTime: extractTimeFromMoment(endMoment),
    }),
  })
}

export const updateEvent = ({id, title: name, description: desc, color, image: photo, startDate, endDate}) => {
  const startMoment = moment(startDate)
  const endMoment = moment(endDate)
  let body = {
    name,
    desc,
    color,
    startDate: extractDateFromMoment(startMoment),
    startTime: extractTimeFromMoment(startMoment),
    endDate: extractDateFromMoment(endMoment),
    endTime: extractTimeFromMoment(endMoment),
  }

  if (typeof photo === 'object') {
    body = {...body, photo}
  }

  return post(`/events/${id}`, {
    begin: EVENTS_UPDATE_REQUEST,
    success: EVENTS_UPDATE_SUCCESS,
    error: EVENTS_UPDATE_FAILURE,
    body: createFormData(body),
  })
}

export const deleteEvent = (id) => del(`/events/${id}`, {
  begin: EVENTS_DELETE_REQUEST,
  success: EVENTS_DELETE_SUCCESS,
  error: EVENTS_DELETE_FAILURE,
  params: {id},
})
