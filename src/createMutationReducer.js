import {handleActions} from 'redux-actions'

const createMutationReducer = ({request, success, failure, ...rest}) => {
  const initialState = {
    pending: false,
    request: null,
    result: null,
    error: null,
  }

  return handleActions({
    [request]: (state, action) => ({
      pending: true,
      request: action.payload,
      result: state.result,
      error: null,
    }),
    [success]: (state, action) => ({
      pending: false,
      request: state.request,
      result: action.payload.body || true,
      error: null,
    }),
    [failure]: (state, action) => ({
      pending: false,
      request: state.request,
      result: false,
      error: action.payload && action.payload.body || action.payload,
    }),
    ...rest,
  }, initialState)
}

export default createMutationReducer
