const createRange = (map) => (min, max, step = 1) => {
  const mapped = []
  for (let c = min, i = 0; c <= max; c += step, ++i) {
    mapped.push(map(c, i))
  }
  return mapped
}

export default createRange
