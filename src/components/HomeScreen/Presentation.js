import React from 'react'
import FormControl from "@material-ui/core/es/FormControl/FormControl"
import Select from "@material-ui/core/es/Select/Select"
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem"
import Calendar from "../Calendar"

export default class HomeScreen extends React.PureComponent {
  state = {
    date: new Date(2018, 7, 1),
    timeRange: {
      min: {
        hours: 8,
        minutes: 0,
      },
      max: {
        hours: 22,
        minutes: 0,
      },
    },
  }

  componentDidMount() {
    const month = this.state.date.getMonth()
    this.props.fetchEvents(month)
    this.props.fetchEvents(month + 1)
    this.props.fetchEvents(month + 2)
  }

  render() {
    const {locale, list, createEvent, updateEvent, deleteEvent} = this.props
    const {timeRange, date} = this.state

    return (
      <div style={{padding: 10}}>
        <Calendar
          timeRange={timeRange}
          date={date}
          events={list || []}
          onDateChange={this.handleDateChange}
          onEventCreate={createEvent}
          onEventUpdate={updateEvent}
          onEventDelete={deleteEvent}
        >
          <FormControl>
            <Select
              value={locale}
              onChange={this.handleLocaleChange}
              inputProps={{name: 'locale', id: 'locale'}}
            >
              <MenuItem value={'en_US'}>English (US)</MenuItem>
              <MenuItem value={'ka'}>ქართული</MenuItem>
            </Select>
          </FormControl>
        </Calendar>
      </div>
    )
  }

  handleLocaleChange = (e) => this.props.setDefaultLocale(e.target.value)

  handleDateChange = (date, prevDate) => {
    if (date > prevDate) {
      this.props.fetchEvents(date.getMonth() + 2)
    } else {
      this.props.fetchEvents(date.getMonth())
    }

    this.setState({
      date,
    })
  }
}
