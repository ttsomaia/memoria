import {DAY} from '../constants'
import moment from "moment"

const generateDays = (timestamp, numberOfDays, initialValue = 0, onDoubleClick, onMouseUp, className) => {
  const days = []
  for (let i = 1; i <= numberOfDays; ++i, timestamp += DAY) {
    const date = moment(timestamp)
    days.push({
      date,
      timestamp,
      className,
      onDoubleClick: onDoubleClick && ((e) => onDoubleClick(e, date)),
      onMouseUp: onMouseUp && ((e) => onMouseUp(e, date)),
    })
  }

  return days
}

export default generateDays
