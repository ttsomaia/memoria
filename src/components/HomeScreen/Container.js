import {connect} from "react-redux"
import {getTranslator, getDefaultLocale, setDefaultLocale} from "redux-l10n"
import Presentation from "./Presentation"
import {fetchEvents, createEvent, updateEvent, deleteEvent, getEvents, getEventsError, setPreference} from "../../ducks"

const mapStateToProps = (state) => ({
  translate: getTranslator(state),
  locale: getDefaultLocale(state),
  list: getEvents(state),
  error: getEventsError(state),
})

const mapDispatchToProps = (dispatch) => ({
  fetchEvents: (month) => dispatch(fetchEvents(month)),
  createEvent: (event) => dispatch(createEvent(event)),
  updateEvent: (event) => dispatch(updateEvent(event)),
  deleteEvent: (id) => dispatch(deleteEvent(id)),
  setLocale: (locale) => dispatch(setPreference('locale', locale)),
  setDefaultLocale: (locale) => dispatch(setDefaultLocale(locale)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Presentation)
