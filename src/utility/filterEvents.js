const filterEvents = (events, filter) => {
  const filteredEvents = {}

  for (const key of Object.keys(events)) {
    filteredEvents[key] = events[key].filter(filter)
  }

  return filteredEvents
}

export default filterEvents
