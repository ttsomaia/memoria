import {createRequestHandlerMiddleware, handleFetch} from "redux-net"
import {transformRequest, transformResponse} from "./transformers"

export default createRequestHandlerMiddleware(
  handleFetch,
  transformRequest,
  transformResponse,
)
