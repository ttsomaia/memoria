const getNumberOfDaysInMonth = (year, month) =>
  new Date(year, month + 1, 0).getDate()

export default getNumberOfDaysInMonth
