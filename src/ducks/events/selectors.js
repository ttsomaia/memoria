export const isEventListLoading = (state) => state.events.list.loading
export const getEvents = (state) => state.events.list.data
export const getEventsError = (state) => state.events.list.error
