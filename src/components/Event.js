import React from 'react'
import * as classNames from 'classnames'

const Event = ({temporary, title, color, isFirstDay, isLastDay, className, style, onMouseDown, onDoubleClick}) => (
  <div
    title={title}
    style={style}
    className={classNames({
      'event': true,
      'event-first-day': isFirstDay,
      'event-last-day': isLastDay,
      'event-temporary': temporary,
      [className]: !!className,
    })}
    onMouseDown={onMouseDown}
    onDoubleClick={onDoubleClick}
  >
    <div className={`event-inner event-${color}`}>
      {isFirstDay && title}
    </div>
  </div>
)

export default Event
