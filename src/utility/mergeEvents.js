import getSortedIndex from './getSortedIndex'
import compareEventNodes from './compareEventNodes'

const mergeEvents = (oldEvents, map) => {
  const result = {...oldEvents}
  let first = true
  let offset

  for (const time of Object.keys(map)) {
    if (!result[time]) {
      result[time] = [map[time]]
      if (first) {
        offset = 0
        first = false
      }
    } else {
      const events = result[time].slice()
      const event = map[time]
      if (first) {
        offset = getSortedIndex(events, event, compareEventNodes)
        first = false
      }
      events.push(event)
      result[time] = [
        ...result[time].slice(0, offset),
        map[time],
        ...result[time].slice(offset),
      ]
    }
  }

  return result
}

export default mergeEvents
