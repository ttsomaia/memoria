import {connect} from "react-redux"
import {getTranslator} from "redux-l10n"
import Calendar from "./Presentation"

const mapStateToProps = (state) => ({
  translate: getTranslator(state),
})

export default connect(
  mapStateToProps,
)(Calendar)
