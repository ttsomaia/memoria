const transformResponse = (response) => response.body instanceof Blob ? response : {
  ...response,
  body: response.body,
}

export default transformResponse
