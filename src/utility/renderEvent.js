import React from 'react'
import {Event} from '../components'
import {DAY} from '../constants'

const renderEvent = (event, onMouseDown, onDoubleClick, temporary) => {
  const {id, startDate, endDate} = event
  const date = startDate.toDate()
  const endTimestamp = +endDate
  const map = {}
  let timestamp = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime()

  for (let k = 0; timestamp < endTimestamp; ++k) {
    map[timestamp] = (
      <Event
        key={id + '@' + k}
        temporary={temporary}
        id={event.id}
        title={event.title}
        description={event.description}
        color={event.color}
        image={event.image}
        startDate={startDate}
        endDate={endDate}
        startTimestamp={+date}
        endTimestamp={endTimestamp}
        isFirstDay={k === 0}
        isLastDay={(timestamp += DAY) > endTimestamp}
        onMouseDown={onMouseDown && ((e) => onMouseDown(e, event))}
        onDoubleClick={onDoubleClick && ((e) => onDoubleClick(e, event))}
      />
    )
  }

  return map
}

export default renderEvent
