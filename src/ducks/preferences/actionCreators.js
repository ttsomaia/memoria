import {
  PREFERENCES_SINGLE_SET,
  PREFERENCES_MANY_SET,
} from "./actionTypes"

export const setPreference = (name, value) => ({
  type: PREFERENCES_SINGLE_SET,
  payload: {
    name,
    value,
  },
})

export const setPreferences = (map) => ({
  type: PREFERENCES_MANY_SET,
  payload: map,
})
