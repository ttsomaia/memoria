import React from 'react'
import Day from '../components/Day'
import {DAY} from '../constants'

const renderDays = (time, className, events, numberOfDays, initialValue = 0) => {
  const days = []

  for (let i = 1; i <= numberOfDays; ++i, time += DAY) {
    days.push(
      <Day
        key={time}
        date={new Date(time)}
        className={className}
      >
        {events[time]}
      </Day>
    )
  }

  return days
}

export default renderDays
