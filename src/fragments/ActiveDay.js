import React from 'react'

const ActiveDay = (day) => (
  <div key={day} className={'day day-active'}>
    <div className={'day-number'}>{day}</div>
  </div>
)

export default ActiveDay
