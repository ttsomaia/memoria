import {
  PREFERENCES_SINGLE_SET,
  PREFERENCES_MANY_SET,
} from './actionTypes'
import {handleActions} from "redux-actions"

const initialState = {}

export default handleActions({
  [PREFERENCES_SINGLE_SET]: (state, {payload: {name, value}}) => ({
    ...state,
    [name]: value,
  }),
  [PREFERENCES_MANY_SET]: (state, action) => ({
    ...state,
    ...action.payload,
  }),
}, initialState)
