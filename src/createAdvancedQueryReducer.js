import {handleActions} from 'redux-actions'
import {mapObject} from './helpers'

const createAdvancedQueryReducer = ({request, receive, error, invalidate = '', initiallyLoading = false, ...rest}, initialData = void 0) => (mergeData) => {
  const initialState = {
    loading: initiallyLoading,
    request: null,
    data: initialData,
    error: null,
  }

  const handlers = {
    [request]: (state, action) => ({
      loading: true,
      request: action.payload,
      data: state.data,
      error: null,
    }),
    [receive]: (state, action) => ({
      loading: false,
      request: state.request,
      data: mergeData(state.data, action.payload.body, state.request),
      error: null,
    }),
    [error]: (state, action) => ({
      loading: false,
      request: state.request,
      data: state.data,
      error: action.payload && action.payload.body || action.payload,
    }),
    ...(invalidate ? {
      [invalidate]: () => initialState,
    } : {}),
  }

  return handleActions({
    ...handlers,
   ...mapObject(rest, handler => typeof handler === 'function' ? (state, action) => ({
     ...state,
     data: handler(state.data, action),
   }) : handlers[handler]),
 }, initialState)
}

export default createAdvancedQueryReducer
