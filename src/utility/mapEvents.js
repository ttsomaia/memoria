const mapEvents = (events, map) => {
  const mappedEvents = {}

  for (const key of Object.keys(events)) {
    mappedEvents[key] = events[key].map(map)
  }

  return mappedEvents
}

export default mapEvents
