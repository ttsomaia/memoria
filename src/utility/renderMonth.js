import getNumberOfDaysInMonth from './getNumberOfDaysInMonth'
import renderDays from './renderDays'

const renderMonth = (date, events) => {
  const time = date.getTime()
  const startingDayOfWeek = date.getDay()
  const year = date.getFullYear()
  const month = date.getMonth()
  const numberOfDays = getNumberOfDaysInMonth(year, month)
  const previousInitialDay = getNumberOfDaysInMonth(year, month - 1) - (startingDayOfWeek - 1)
  const previousTime = new Date(year, month - 1, previousInitialDay).getTime()
  const nextTime = new Date(year, month + 1, 1).getTime()

  return [
    ...renderDays(
      previousTime,
      'day-inactive',
      events,
      startingDayOfWeek,
      getNumberOfDaysInMonth(year, month - 1) - startingDayOfWeek,
    ),
    ...renderDays(
      time,
      'day-active',
      events,
      numberOfDays,
    ),
    ...renderDays(
      nextTime,
      'day-inactive',
      events,
      (7 - ((numberOfDays + startingDayOfWeek) % 7)) % 7,
    ),
  ]
}

export default renderMonth
