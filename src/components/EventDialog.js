import React from 'react'
import Dialog from "@material-ui/core/es/Dialog/Dialog"
import DialogTitle from "@material-ui/core/es/DialogTitle/DialogTitle"
import DialogContent from "@material-ui/core/es/DialogContent/DialogContent"
import TextField from "@material-ui/core/es/TextField/TextField"
import DialogActions from "@material-ui/core/es/DialogActions/DialogActions"
import Button from "@material-ui/core/es/Button/Button"
import DatePicker from 'material-ui-pickers/DatePicker'
import TimePicker from 'material-ui-pickers/TimePicker'
import FormControl from "@material-ui/core/es/FormControl/FormControl"
import InputLabel from "@material-ui/core/es/InputLabel/InputLabel"
import Select from "@material-ui/core/es/Select/Select"
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem"
import Avatar from "@material-ui/core/es/Avatar/Avatar"
import PictureInPicture from '@material-ui/icons/Image'
import ButtonBase from "@material-ui/core/es/ButtonBase/ButtonBase"

export default class EventDialog extends React.PureComponent {
  file
  state = {
    image: '',
    deleteDialogOpen: false,
  }

  constructor(props) {
    super(props)
    this.state.image = this.props.image
  }

  componentDidUpdate(prevProps) {
    if (this.props.values.image !== prevProps.values.image) {
      const image = this.props.values.image

      if (typeof image === 'string') {
        this.setState({
          image,
        })
      }
    }
  }

  render() {
    const {translate, open, values = {}, onClose} = this.props
    const {image, deleteDialogOpen} = this.state
    const imageSrc = image

    return (
      <Dialog open={open} onClose={onClose} aria-labelledby="event-dialog-title" disableEnforceFocus >
        <DialogTitle id="event-dialog-title">{values.id ? translate('edit_event') : translate('create_event')}</DialogTitle>
        <DialogContent>
          <div className={'event-dialog-row'}>
            <FormControl fullWidth margin={'dense'}>
              <DatePicker
                emptyLabel={translate('start_date')}
                cancelLabel={translate('cancel')}
                okLabel={translate('ok')}
                format={'DD/MM/YYYY'}
                autoOk={true}
                value={values.startDate}
                onChange={this.handleStartDateChange}
              />
            </FormControl>

            <FormControl fullWidth margin={'dense'}>
              <TimePicker
                emptyLabel={translate('start_time')}
                cancelLabel={translate('cancel')}
                okLabel={translate('ok')}
                format={'HH:mm'}
                autoOk={true}
                value={values.startDate}
                onChange={this.handleStartDateChange}
              />
            </FormControl>
          </div>

          <div className={'event-dialog-row'}>
            <FormControl fullWidth margin={'dense'}>
              <DatePicker
                emptyLabel={translate('end_date')}
                cancelLabel={translate('cancel')}
                okLabel={translate('ok')}
                format={'DD/MM/YYYY'}
                autoOk={true}
                value={values.endDate}
                onChange={this.handleEndDateChange}
              />
            </FormControl>

            <FormControl fullWidth margin={'dense'}>
              <TimePicker
                emptyLabel={translate('end_date')}
                cancelLabel={translate('cancel')}
                okLabel={translate('ok')}
                format={'HH:mm'}
                autoOk={true}
                value={values.endDate}
                onChange={this.handleEndDateChange}
              />
            </FormControl>
          </div>

          <TextField
            fullWidth
            margin={'dense'}
            label={translate('title')}
            type={'text'}
            name={'title'}
            value={values.title}
            onChange={this.handleValueChange}
          />

          <TextField
            fullWidth
            multiline
            margin={'dense'}
            rows={4}
            label={translate('description')}
            type={'text'}
            name={'description'}
            value={values.description}
            onChange={this.handleValueChange}
          />

          <div className={'event-dialog-row'}>
            <FormControl fullWidth margin={'dense'}>
              <InputLabel htmlFor="color">{translate('color')}</InputLabel>
              <Select
                value={values.color}
                onChange={this.handleValueChange}
                inputProps={{name: 'color', id: 'color'}}
              >
                <MenuItem value={'blue'}>{translate('blue')}</MenuItem>
                <MenuItem value={'red'}>{translate('red')}</MenuItem>
                <MenuItem value={'green'}>{translate('green')}</MenuItem>
                <MenuItem value={'yellow'}>{translate('yellow')}</MenuItem>
              </Select>
            </FormControl>

            <ButtonBase
              focusRipple
              classes={classes.uploadButton}
              onClick={this.openFileBrowser}
            >
              <Avatar
                alt="Image"
                src={imageSrc}
                className={`avatar event-${values.color || 'blue'}`}
              >
                {!imageSrc && (<PictureInPicture/>)}
              </Avatar>
              <input
                type={'file'}
                name={'image'}
                accept={'image/*'}
                className={'hidden'}
                onChange={this.handleFileChange}
                ref={this.receiveFileReference}
              />
            </ButtonBase>
          </div>

          <Dialog open={deleteDialogOpen} onClose={this.handleDeleteDialogClose} aria-labelledby="delete-event-dialog-title" disableEnforceFocus fullWidth maxWidth={'sm'}>
            <DialogTitle id="delete-event-dialog-title">{translate('confirm_delete_event')}</DialogTitle>
            <DialogContent>
              {translate('confirm_delete_event_message')}
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleDeleteDialogClose} color="primary">{translate('no')}</Button>
              <Button onClick={this.handleDeleteDialogConfirm} color="primary">{translate('yes')}</Button>
            </DialogActions>
          </Dialog>

        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">{translate('cancel')}</Button>
          <Button onClick={this.handleSubmit} color="primary">{translate('save')}</Button>
          {values.id && (
            <Button onClick={this.handleDeleteDialogOpen} color="secondary">{translate('delete')}</Button>
          )}
        </DialogActions>
      </Dialog>
    )
  }

  handleStartDateChange = (value) => {
    const endDate = this.props.values.endDate
    const {min, max} = this.props.timeRange || {}
    value = this.transformTime(value, min, max)
    this.props.onValueChange('startDate', value > endDate ? endDate.clone() : value)
  }

  handleEndDateChange = (value) => {
    const startDate = this.props.values.startDate
    const {min, max} = this.props.timeRange || {}
    value = this.transformTime(value, min, max)
    this.props.onValueChange('endDate', value < startDate ? startDate.clone() : value)
  }

  handleValueChange = ({target}) => this.props.onValueChange(target.name, target.value)

  transformTime(value, min, max) {
    const hours = value.hours()
    const minutes = value.minutes()

    if ((!min && !max) || (hours >= min.hours && hours <= max.hours && (hours > min.hours || minutes >= min.minutes) && (hours < max.hours || minutes <= max.minutes))) {
      return value
    }

    value = value.clone()
    min && hours < min.hours && value.hours(min.hours)
    max && hours > max.hours && value.hours(max.hours)
    min && hours <= min.hours && minutes < min.minutes && value.minutes(min.minutes)
    max && hours >= max.hours && minutes > max.minutes && value.minutes(max.minutes)
    return value
  }

  openFileBrowser = () => {
    this.file.click()
  }

  handleFileChange = ({target: input}) => {
    if (input.files && input.files[0]) {
      this.loadImage(input.files[0])
      this.props.onValueChange('image', input.files[0])
    }
  }

  loadImage = (file) => {
    const reader = new FileReader()
    reader.onload = this.handleImageLoad
    reader.readAsDataURL(file)
  }

  handleImageLoad = (e) => this.setState({
    image: e.target.result,
  })

  receiveFileReference = (ref) => {
    this.file = ref
  }

  handleSubmit = () => {
    this.props.onSubmit(this.props.values)
    this.props.onClose()
  }

  handleDeleteDialogOpen = () => this.setState({
    deleteDialogOpen: true,
  })

  handleDeleteDialogClose = () => this.setState({
    deleteDialogOpen: false,
  })

  handleDeleteDialogConfirm = () => {
    this.props.onDelete && this.props.onDelete(this.props.values.id)
    this.handleDeleteDialogClose()
    this.props.onClose()
  }
}

const classes = {
  uploadButton: {
    root: 'upload-button',
  },
}
