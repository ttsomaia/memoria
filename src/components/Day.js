import React from 'react'
import TableCell from "@material-ui/core/es/TableCell/TableCell"
import classNames from 'classnames'

const Day = ({date, className, events, onDoubleClick, onMouseUp}) => (
  <TableCell padding={'none'} classes={rootClasses} numeric>
    <div className={classNames('day', className)} onDoubleClick={onDoubleClick} onMouseUp={onMouseUp}>
      <div className={'day-number'}>{date.date()}</div>
      {events}
    </div>
  </TableCell>
)

const rootClasses = {root: 'day-cell'}

export default Day
