import {compose, withAcceptLanguage, withCors, withFilledUrl, withRequestHeaders, withResponseType, stringifyUrl} from 'redux-net'
import {getApiHost, getApiScheme, getLocale} from "./../selectors"

const withCondition = (predicate) => (transformer) => (request, state) => predicate(request, state) ? transformer(request, state) : request
const wheneverIsLocalRequest = withCondition((request, state) => !request.url.host || request.url.host === getApiHost(state))
const wheneverIsPostOrPut = withCondition((request) => request.method === 'POST' || request.method === 'PUT')

const transformRequest = compose(
  (request) => {
    console.log(request.method, stringifyUrl(request.url), request.body)
    return request
  },
  withCors,
  withResponseType('json'),
  wheneverIsLocalRequest(withAcceptLanguage(getLocale)),
  wheneverIsPostOrPut(withRequestHeaders({
    'Accept': 'application/json, text/plain, */*',
  })),
  withFilledUrl((state) => ({
    scheme: getApiScheme(state),
    host: getApiHost(state),
  })),
  (request) => !request.body || typeof request.body !== 'object' || request.body.constructor === FormData ? request : {
    ...request,
    body: JSON.stringify(request.body),
  },
)

export default transformRequest
