import createAdvancedQueryReducer from './createAdvancedQueryReducer'

const mergeData = (prevData, newData) => newData

const createQueryReducer = (handlers, initialData = null, merge = mergeData) => createAdvancedQueryReducer(
  handlers,
  initialData,
)(merge)

export default createQueryReducer
