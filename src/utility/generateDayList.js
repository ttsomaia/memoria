import memoize from './memoize'

const generateDayList = (year, month, numberOfDays, initialValue = 0) => {
  const days = []

  for (let day = 1; day <= numberOfDays; ++day) {
    days.push(initialValue + day)
  }

  return days
}

export default memoize(generateDayList)
