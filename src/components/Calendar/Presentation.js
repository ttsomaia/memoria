import React from 'react'
import Button from "@material-ui/core/es/Button/Button"
import ChevronLeft from "@material-ui/icons/ChevronLeft"
import ChevronRight from "@material-ui/icons/ChevronRight"
import Typography from "@material-ui/core/es/Typography/Typography"
import Table from "@material-ui/core/es/Table/Table"
import TableHead from "@material-ui/core/es/TableHead/TableHead"
import TableRow from "@material-ui/core/es/TableRow/TableRow"
import TableCell from "@material-ui/core/es/TableCell/TableCell"
import TableBody from "@material-ui/core/es/TableBody/TableBody"
import moment from "moment"
import Event from '../Event'
import Day from "../Day"
import DayDialog from '../DayDialog'
import EventDialog from "../EventDialog"
import {
  filterEvents,
  generateCalendarMatrix,
  mergeEvents,
  renderEvent,
  renderEvents,
  range, createRange,
} from '../../utility'

class Calendar extends React.Component { //@todo: don't update event when it is dropped in the same day
  state = {
    weekRange: range(0, 6),
    eventDialog: {
      open: false,
      initialValues: {
        id: '',
        title: '',
        description: '',
        color: 'blue',
        image: '',
      },
    },
    dayDialog: {
      open: false,
      title: '',
    },
    rows: [1, 2, 3, 4, 5, 6],
    draggingEvent: null,
    draggingEventPosition: null,
    draggingEventDelta: null,
  }

  constructor(props) {
    super(props)
    const {timeRange} = props || {}
    this.state.date = props.date
    this.state.days = generateCalendarMatrix(props.date, this.handleDayDoubleClick, this.handleDayMouseUp)
    this.state.hours = timeRange.min && (timeRange.min.hours || 12)
    this.state.eventDialog.values = this.state.eventDialog.initialValues
    this.state.renderedEvents = renderEvents(
      props.events.map(this.mapEventToMomentEvent),
      this.handleEventMouseDown,
      this.handleEventDoubleClick,
    )

    const eventMap = {}
    for (const event of props.events) {
      eventMap[event.id] = event
    }

    this.state.eventMap = eventMap
  }

  mapEventToMomentEvent({startDate, endDate, ...event}) {
    return {
      ...event,
      startDate: moment(startDate),
      endDate: moment(endDate),
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.events !== prevProps.events) {
      let renderedEvents = {...this.state.renderedEvents}
      const prevEventMap = this.state.eventMap
      const eventMap = {}
      for (let event of this.props.events) {
        const prevEvent = prevEventMap[event.id]
        eventMap[event.id] = event
        if (!prevEvent || event !== prevEvent) {
          event = this.mapEventToMomentEvent(event)
          const id = event.id
          renderedEvents = mergeEvents(
            filterEvents(renderedEvents, !prevEvent ? this.isNotTemporary : (e => e.props.id !== id)),
            renderEvent(event, this.handleEventMouseDown, this.handleEventDoubleClick),
          )
        }
      }

      for (let event of prevProps.events) {
        const id = event.id
        if (!eventMap[event.id]) {
          renderedEvents = filterEvents(renderedEvents, e => e.props.id !== id)
        }
      }

      const date = this.props.date
      this.setState({
        date,
        eventMap,
        renderedEvents,
        days: date === prevProps.date
          ? this.state.days
          : generateCalendarMatrix(date, this.handleDayDoubleClick, this.handleDayMouseUp),
      })
    } else if (+this.props.date !== +prevProps.date) {
      this.showMonthForDate(this.props.date)
    }
  }

  render() {
    const {translate, timeRange, children} = this.props
    const {renderedEvents, dayDialog, eventDialog, date, rows, draggingEvent, draggingEventPosition} = this.state

    return (
      <div onMouseMove={this.handleMouseMove} onMouseUp={this.handleMouseUp}>
        <EventDialog
          translate={translate}
          timeRange={timeRange}
          open={eventDialog.open}
          values={eventDialog.values}
          onValueChange={this.handleEventValueChange}
          onSubmit={this.submitEvent}
          onDelete={this.deleteEvent}
          onClose={this.closeEventDialog}
        />

        <DayDialog
          translate={translate}
          open={dayDialog.open}
          date={dayDialog.date}
          timestamp={dayDialog.timestamp}
          timeRange={timeRange}
          title={dayDialog.title}
          events={renderedEvents[dayDialog.timestamp]}
          onEventDoubleClick={this.handleEventDoubleClick}
          onCreateEventClick={this.handleCreateEventClick}
          onClose={this.closeDayDialog}
        />

        <div className={'panel'}>
          <div className={'buttons'}>
            <Button color="primary" size={'small'} onClick={this.showPreviousMonth} title={translate('back')}>
              <ChevronLeft/>
            </Button>
            <Button color="primary" size={'small'} onClick={this.showCurrentMonth}>{translate('today')}</Button>
            <Button color="primary" size={'small'} onClick={this.showNextMonth} title={translate('next')}>
              <ChevronRight/>
            </Button>
            {children}
          </div>

          <div className={'title'}>
            <Typography variant={'headline'}>{translate('month_year', date)}</Typography>
          </div>
        </div>

        <Table>
          <TableHead>
            <TableRow>
              <TableCell numeric>{translate('sunday')}</TableCell>
              <TableCell numeric>{translate('monday')}</TableCell>
              <TableCell numeric>{translate('tuesday')}</TableCell>
              <TableCell numeric>{translate('wednesday')}</TableCell>
              <TableCell numeric>{translate('thursday')}</TableCell>
              <TableCell numeric>{translate('friday')}</TableCell>
              <TableCell numeric>{translate('saturday')}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(this.renderTableRow)}
          </TableBody>
        </Table>
        {draggingEvent && (
          <Event
            data={draggingEvent}
            title={draggingEvent.title}
            color={draggingEvent.color}
            isFirstDay={true}
            isLastDay={true}
            className={'draggable'}
            style={draggingEventPosition}
          />
        )}
      </div>
    )
  }

  isNotTemporary(e) {
    return !e.props.temporary
  }

  renderTableRow = (v, i) => {
    const from = i * 7

    return (
      <TableRow key={i}>
        {this.renderDays(from, from + 6)}
      </TableRow>
    );
  }

  renderDay = (offset) => {
    const props = this.state.days[offset]

    return (
      <Day
        key={props.timestamp}
        date={props.date}
        events={this.state.renderedEvents[props.timestamp]}
        className={props.className}
        onDoubleClick={props.onDoubleClick}
        onMouseUp={props.onMouseUp}
      />
    )
  }

  renderDays = createRange(this.renderDay)

  handleCreateEventClick = (date) => this.openEventDialog({
    startDate: date.clone().hours(this.state.hours).minutes(0),
    endDate: date.clone().hours(this.state.hours + 1).minutes(0),
  })

  handleDayDoubleClick = (e, date) => {
    const timestamp = +date
    const events = this.state.renderedEvents[timestamp]
    return events && events.length > 0
      ? this.openDayDialog(date, timestamp)
      : this.openEventDialog({
        startDate: moment(timestamp).hours(this.state.hours).minutes(0),
        endDate: moment(timestamp).hours(this.state.hours + 1).minutes(0),
      })
  }

  handleDayMouseUp = (e, date) => {
    e.stopPropagation()

    if (!this.state.draggingEvent) {
      if (this.holdTimeout) {
        clearTimeout(this.holdTimeout)
        this.holdTimeout = null
      }
      return
    }

    const {startDate, endDate, ...event} = this.state.draggingEvent
    const newStartDate = date.clone().hours(startDate.hours()).minutes(startDate.minutes()).seconds(startDate.seconds()).milliseconds(startDate.milliseconds())
    const newEndDate = moment(newStartDate + (endDate - startDate))
    this.insertTemporaryEvent({
      ...event,
      startDate: newStartDate,
      endDate: moment(newStartDate + (endDate - startDate))
    })
    this.props.onEventUpdate && this.props.onEventUpdate({
      ...event,
      startDate: newStartDate.format(),
      endDate: newEndDate.format(),
    })
  }

  openDayDialog = (date, timestamp) => this.setState({
    dayDialog: {
      ...this.state.dayDialog,
      date,
      timestamp,
      title: this.props.translate('day_month_year', date),
      open: true,
    },
  })

  closeDayDialog = () => this.setState({
    dayDialog: {
      ...this.state.dayDialog,
      open: false,
    },
  })

  handleEventDoubleClick = (e, event) => {
    e.stopPropagation()
    this.setState({
      eventDialog: {
        ...this.state.eventDialog,
        values: event,
        open: true,
      },
    })
    return false
  }

  openEventDialog = (data = {}) => this.setState({
    eventDialog: {
      ...this.state.eventDialog,
      values: {
        ...this.state.eventDialog.initialValues,
        ...data,
      },
      open: true,
    },
  })

  closeEventDialog = () => this.setState({
    eventDialog: {
      ...this.state.eventDialog,
      open: false,
    },
  })

  handleEventValueChange = (name, value) => this.setState({
    eventDialog: {
      ...this.state.eventDialog,
      values: {
        ...this.state.eventDialog.values,
        [name]: value,
      },
    },
  })

  showPreviousMonth = () => {
    const currentDate = this.state.date
    this.showMonthForDate(new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 1))
  }

  showNextMonth = () => {
    const currentDate = this.state.date
    this.showMonthForDate(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1))
  }

  showCurrentMonth = () => {
    const date = new Date()
    this.showMonthForDate(new Date(date.getFullYear(), date.getMonth(), 1))
  }

  showMonthForDate(date) {
    if (+date === +this.state.date) {
      return
    }

    this.props.onDateChange && this.props.onDateChange(date, this.state.date)
    this.setState({
      date,
      days: generateCalendarMatrix(date, this.handleDayDoubleClick, this.handleDayMouseUp),
    })
  }

  handleEventMouseDown = ({clientY, clientX, target}, event) => {
    this.holdTimeout = setTimeout(() => {
      this.holdTimeout = null
      const id = event.id
      const renderedEvents = filterEvents(this.state.renderedEvents, e => e.props.id !== id)
      const offsetTop = target.offsetParent.offsetTop + target.offsetTop
      const offsetLeft = target.offsetParent.offsetLeft + target.offsetLeft
      this.setState({
        renderedEvents,
        draggingEvent: event,
        draggingEventPosition: {
          top: offsetTop,
          left: offsetLeft,
          width: target.offsetWidth,
        },
        draggingEventDelta: {
          top: clientY - offsetTop,
          left: clientX - offsetLeft,
        },
      })
    }, 150)
  }

  handleMouseMove = ({nativeEvent}) => this.state.draggingEvent && this.setState({
    draggingEventPosition: {
      top: nativeEvent.clientY - this.state.draggingEventDelta.top,
      left: nativeEvent.clientX - this.state.draggingEventDelta.left,
      width: this.state.draggingEventPosition.width,
    },
  })

  handleMouseUp = () => {
    if (!this.state.draggingEvent) {
      if (this.holdTimeout) {
        clearTimeout(this.holdTimeout)
        this.holdTimeout = null
      }
      return
    }

    this.insertEvent(this.state.draggingEvent)
  }

  submitEvent = ({id, title, description, color, image, startDate, endDate}) => {
    id = +id

    if (!id) {
      const event = {
        title,
        description,
        color,
        image,
        startDate,
        endDate,
      }
      this.insertTemporaryEvent({
        ...event,
        id: Date.now() + '_' + Math.random()
      })

      return this.props.onEventCreate && this.props.onEventCreate(event)
    }

    const event = {
      id,
      title,
      description,
      color,
      image,
      startDate,
      endDate,
    }
    this.insertTemporaryEvent(event, filterEvents(this.state.renderedEvents, e => e.props.id !== id))
    this.props.onEventUpdate && this.props.onEventUpdate(event)
  }

  deleteEvent = (id) => {
    const event = this.props.events.find(e => e.id === id)
    this.insertTemporaryEvent({
      ...event,
      startDate: moment(event.startDate),
      endDate: moment(event.endDate),
    }, filterEvents(this.state.renderedEvents, e => e.props.id !== id))
    this.props.onEventDelete && this.props.onEventDelete(id)
  }

  insertTemporaryEvent(event, prevEvents) {
    const events = renderEvent(event, this.handleEventMouseDown, this.handleEventDoubleClick, true)
    const renderedEvents = mergeEvents(prevEvents || this.state.renderedEvents, events)
    this.setState({
      renderedEvents,
      draggingEvent: null,
      draggingEventPosition: null,
    })
  }

  insertEvent(event, prevEvents) {
    const events = renderEvent(event, this.handleEventMouseDown, this.handleEventDoubleClick)
    const renderedEvents = mergeEvents(prevEvents || this.state.renderedEvents, events)
    this.setState({
      renderedEvents,
      draggingEvent: null,
      draggingEventPosition: null,
    })
  }
}

export default Calendar
