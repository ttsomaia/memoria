import React from 'react'

const createDay = (events, className, draggingEventData, year, month) => (day) => {

  return (
    <div key={day} className={'day ' + className} data-year={year} data-month={month} data-day={day}>
      <div className={'day-number'}>{day}</div>
      {events[day] && (!draggingEventData ? events[day] : events[day].filter(event => event.props.data.id != draggingEventData.id))}
    </div>
  )
}

export default createDay
