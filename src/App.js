import React from 'react'
import {Provider} from "react-redux"
import {PersistGate} from 'redux-persist/integration/react'
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider'
import MomentUtils from 'material-ui-pickers/utils/moment-utils'
import {HomeScreen} from './components'
import store from "./store"
import persistor from "./persistor"
import './assets/layout.css'
import './assets/default.theme.css'

const App = () => (
  <Provider store={store}>
    <PersistGate loading={<div>Loading...</div>} persistor={persistor}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
          <HomeScreen/>
      </MuiPickersUtilsProvider>
    </PersistGate>
  </Provider>
)

export default App
