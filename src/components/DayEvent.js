import React from 'react'
import TableCell from "@material-ui/core/es/TableCell/TableCell"
import * as classNames from 'classnames'

const DayEvent = ({id, title, color, delta, startDate, endDate, onDoubleClick}) => (
  <TableCell key={id} rowSpan={delta + 1} padding={'none'} classes={classes.cell}>
    <div className={'day-event-wrapper'}>
      <div className={classNames('day-event', 'event-' + color)} onDoubleClick={onDoubleClick}>
        {startDate.format('HH:mm')}-{endDate.format('HH:mm')}<br/>
        {title}
      </div>
    </div>
  </TableCell>
)

const classes = {
  cell: {
    root: 'event-cell',
  },
}

export default DayEvent
